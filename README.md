
# LMS Backend
The repository is about to develop a server-side Rest Api for the Library Management System to handling all the user along with Issue and Return of book. This api is built on the top of Node-Express Framwork with MongoDb as Database.



# Guide To install
Clone this repository into your file 
 or simply download zip file from Github. Make sure you have version greater than 6.

run
 `npm install`
 `nodemon`


 # Url Detail
 Please install postman for better view of Api and for browser view please into JsonViewer.

 for library side<br>
 ```diff
 localhost:3000/books (as a post request for adding book)<br>
 localhost:3000/books (as a get request for getting all book detail)<br>
 localhost:3000/books/bookId (as a get request for getting detial for a book)<br>
 localhost:3000/books/bookId (as a patch request for updating book detail)<br>
 localhost:3000/books/bookId( as a delete request for deleting book)<br>
 ```
 On user side
 ```diff
localhost:3000/auth/signup (as a post request for signing up)<br>
lcoalhost:3000/auth/login( as a post request for loging in ) <br>
```
On Issue and Return Of Book
```diff
localhost:3000/issue/<bookId> ( as a post request for issue book)<br>
localhost:3000/issue/        (as a get request for getting all the book in user account)<br>
localhost:3000/issue/<bookId> (as a delete request for returning book )  <br>
 ```
